import bi_pygtk_extra as g_wh

if __name__ == '__main__':

    g_wh.gdk.threads_init()

    devices= []
    for i in range(10):
        devices.append('Device {: 2d}'.format(i))

    dialog= g_wh.DeviceChooser(devices, 'Devices', None, message='An explanatory text')

    res= dialog.Run()

    print("Result is {:s}".format(str(res)))
    print("\n\nPress Ctrl-C to exit main loop")
    g_wh.main()

