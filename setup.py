"""
setup.py for bi-pygtk-extra.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
import os
from re import match
from pathlib import Path
from setuptools import setup, find_packages


HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt') as fh:
    LONG_DESCRIPTION = fh.read().strip()

examples_dir = 'examples'
example_files = [os.path.join(examples_dir, f) for f in os.listdir(examples_dir)]
package_name = 'bi-pygtk-extra'

info = {}
with open('bi_pygtk_extra/__info__.py') as fp:
    for line in fp.readlines():
        # look for __attribute__ = 'something'
        mo = match(r"^\s*(\w+)\s*=\s*['\"]?([\w .:/@\-+]+)['\"]?\s*$", line)
        if mo is not None:
            info[mo.group(1)] = mo.group(2)

setup(
    name=package_name,
    version=info['__version__'],
    author=info['__author__'],
    author_email=info['__author_email__'],
    license='MIT',
    description=info['__description__'],
    long_description=LONG_DESCRIPTION,
    # long_description_content_type='text/markdown',
    url=info['__url__'],
    packages=find_packages(),
    package_data={'bi_pygtk_extra': ['*.xpm']},
    data_files=[(os.path.join('share', package_name), ['LICENSE.txt']),
                (os.path.join('share', package_name, 'examples'), example_files)],
    python_requires='>=2.7, <4',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=[],
    extras_require={'test': 'pytest'},
)
