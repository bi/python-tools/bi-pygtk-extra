"""
High-level tests for the  package.

"""

import bi_pygtk_extra as wh


def test_version():
    assert wh.__version__ is not None
    assert wh.__version__ >= '0.1.0'

def test_info():
    for a in ('__author__', '__author_email__', '__description__', '__url__'):
        assert wh.__getattribute__(a) is not None

def test_decorators():
    class A(object):
        def __init__(self):
            self.refreshing= False
            self.state= 0

        @wh.callback
        def cb(self):
            self.state+= 1

        @wh.updater
        def update(self):
            self.cb()

    a= A()
    assert a.state == 0
    a.cb()
    assert a.state == 1
    a.update()
    assert a.state == 1
    a.cb()
    assert a.state == 2

def test_combo_box():
    cb= wh.g_cbox_factory(entries=['1 a', '2 b', '3 c'], active=1)
    assert cb.get_active() == 1
    wh.append_combo_box(cb, ['4 d', '5 e'], 2)
    assert cb.get_active() == 2
    it=cb.get_active_iter()
    m= cb.get_model()
    assert m.get_value(it, 0) == '3 c'
    wh.clear_combo_box(cb)
    assert cb.get_active() == -1

def test_factories():
    w= wh.g_window_factory(title= 'A window')
    assert type(w) is wh.Gtk.Window

    w= wh.g_button_factory()
    assert type(w) is wh.Gtk.Button

    w= wh.g_cbutton_factory()
    assert type(w) is wh.Gtk.CheckButton

    w= wh.g_spin_factory()
    assert type(w) is wh.Gtk.SpinButton

    w= wh.g_entry_factory(text='abcd')
    wh.g_update_entry(w, 11.0, '{:10.3f}')
    assert type(w) is wh.Gtk.Entry
    assert w.get_text() == '    11.000'

    f, l= wh.g_label_factory(text='12345', frame=True)
    assert type(l) is wh.Gtk.Label
    assert type(f) is wh.Gtk.Frame
    assert l.get_text() == '12345'
    assert l.get_value() == 12345

    w= wh.g_separator_label_factory()
    assert type(w) is wh.Gtk.HBox
    assert type(w.label) is wh.Gtk.Label

# Gtk.Table is deprecated use Gtk.Grid
#     w= []
#     for i in range(5):
#         w.append([])
#         for j in range(3):
#             w[-1].append(wh.g_label_factory(text='{:d}:{:d}'.format(i,j), frame=True)[0])
#     t= wh.g_table_factory(w)
#     assert type(t) is wh.Gtk.Table

def test_widgets():
    w= wh.LoginDialog()
    assert w is not None

    w= wh.StatusLed()
    assert w is not None
    w.set_active(wh.LED_GREEN)

    w= wh.TimeRangeChooser()
    assert w is not None
    w.SetDate('2019-12-25')
    assert w.GetDate() == '2019-12-25'

    w= wh.DeviceChooser(['dev1', 'dev2', 'dev3'])
    assert w is not None

    w= wh.SingleDeviceChooser(['dev1', 'dev2', 'dev3'])
    assert w is not None

