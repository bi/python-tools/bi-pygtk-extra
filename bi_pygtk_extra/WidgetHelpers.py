#! /usr/bin/env python

try:
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    from gi.repository import Gdk as gdk
    from gi.repository import GObject
    gtk3= True

    ARROW_UP= Gtk.ArrowType.UP
    ARROW_DOWN= Gtk.ArrowType.DOWN
    ARROW_LEFT= Gtk.ArrowType.LEFT
    ARROW_RIGHT= Gtk.ArrowType.RIGHT

    AOPT_EXPAND= Gtk.AttachOptions.EXPAND
    AOPT_FILL= Gtk.AttachOptions.FILL

    POS_TOP= Gtk.PositionType.TOP
    POS_BOTTOM= Gtk.PositionType.BOTTOM
    POS_RIGHT= Gtk.PositionType.RIGHT
    POS_LEFT= Gtk.PositionType.LEFT

    DIALOG_MODAL= Gtk.DialogFlags.MODAL
    DIALOG_DESTROY_WITH_PARENT= Gtk.DialogFlags.DESTROY_WITH_PARENT

    RESPONSE_REJECT= Gtk.ResponseType.REJECT
    RESPONSE_ACCEPT= Gtk.ResponseType.ACCEPT
    RESPONSE_CANCEL= Gtk.ResponseType.CANCEL

    WINDOW_TOPLEVEL                  = Gtk.WindowType.TOPLEVEL

    SHADOW_ETCHED_OUT                = Gtk.ShadowType.ETCHED_OUT
    SHADOW_ETCHED_IN                 = Gtk.ShadowType.ETCHED_IN
    SHADOW_NONE                      = Gtk.ShadowType.NONE

    JUSTIFY_FILL                     = Gtk.Justification.FILL
    SELECTION_MULTIPLE               = Gtk.SelectionMode.MULTIPLE
    SELECTION_SINGLE                 = Gtk.SelectionMode.SINGLE
    SELECTION_BROWSE                 = Gtk.SelectionMode.BROWSE
    FILE_CHOOSER_ACTION_OPEN         = Gtk.FileChooserAction.OPEN
    FILE_CHOOSER_ACTION_SELECT_FOLDER= Gtk.FileChooserAction.SELECT_FOLDER
    FILE_CHOOSER_ACTION_SAVE         = Gtk.FileChooserAction.SAVE
    FILE_CHOOSER_ACTION_CREATE_FOLDER= Gtk.FileChooserAction.CREATE_FOLDER

except ImportError:
    import pygtk
    pygtk.require('2.0')
    import gtk as Gtk
    import gobject as GObject
    gtk3= False
    gdk= Gtk.gdk

    ARROW_UP= Gtk.ARROW_UP
    ARROW_DOWN= Gtk.ARROW_DOWN
    ARROW_LEFT= Gtk.ARROW_LEFT
    ARROW_RIGHT= Gtk.ARROW_RIGHT

    AOPT_EXPAND= Gtk.EXPAND
    AOPT_FILL= Gtk.FILL

    POS_TOP= Gtk.POS_TOP
    POS_BOTTOM= Gtk.POS_BOTTOM
    POS_RIGT= Gtk.POS_RIGHT
    POS_LEFT= Gtk.POS_LEFT

    DIALOG_MODAL= Gtk.DIALOG_MODAL
    DIALOG_DESTROY_WITH_PARENT= Gtk.DIALOG_DESTROY_WITH_PARENT

    RESPONSE_REJECT= Gtk.RESPONSE_REJECT
    RESPONSE_ACCEPT= Gtk.RESPONSE_ACCEPT
    RESPONSE_CANCEL= Gtk.RESPONSE_CANCEL

    WINDOW_TOPLEVEL                  = Gtk.WINDOW_TOPLEVEL

    SHADOW_ETCHED_OUT                = Gtk.SHADOW_ETCHED_OUT
    SHADOW_ETCHED_IN                 = Gtk.SHADOW_ETCHED_IN
    SHADOW_NONE                      = Gtk.SHADOW_NONE

    JUSTIFY_FILL                     = Gtk.JUSTIFY_FILL
    SELECTION_MULTIPLE               = Gtk.SELECTION_MULTIPLE
    SELECTION_SINGLE                 = Gtk.SELECTION_SINGLE
    SELECTION_BROWSE                 = Gtk.SELECTION_BROWSE
    FILE_CHOOSER_ACTION_OPEN         = Gtk.FILE_CHOOSER_ACTION_OPEN
    FILE_CHOOSER_ACTION_SELECT_FOLDER= Gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER
    FILE_CHOOSER_ACTION_SAVE         = Gtk.FILE_CHOOSER_ACTION_SAVE
    FILE_CHOOSER_ACTION_CREATE_FOLDER= Gtk.FILE_CHOOSER_ACTION_CREATE_FOLDER

import os
import datetime as dt
from types import MethodType

def is_string(what):
    return isinstance(what, ("".__class__, u"".__class__))

def main():
    try:
        Gtk.main()
    except KeyboardInterrupt:
        pass

def quit():
    Gtk.main_quit()

################## Widget helpers ##################

# Decorators
def callback(func):
    def wrapped(*args, **kwargs):
        try:
            refreshing= args[0].refreshing
        except:
            refreshing= False

        if refreshing:
            #print "Skipping", str(func)
            return False
        else:
            #print "Executing", str(func)
            return func(*args, **kwargs)

    return wrapped

def updater(func):
    def wrapped(*args, **kwargs):
        args[0].refreshing= True
        res= func(*args, **kwargs)
        args[0].refreshing= False
        return res

    return wrapped

# ComboBox helpers
def populate_combo_box(cbox, entries, active=0):

    lstore = Gtk.ListStore(str)
    for i in entries:
        lstore.append([i])

    cbox.set_model(lstore)
    cell = Gtk.CellRendererText()
    cbox.pack_start(cell, True)
    cbox.add_attribute(cell, 'text', 0)
    cbox.set_active(active)

    #print("populate_combo_box", entries)

    return (lstore, cell)

def clear_combo_box(cbox):
    try:
        lstore= cbox.get_model()
        lstore.clear()
        #print("clear_combo_box")
    except:
        pass

def append_combo_box(cbox, entries, active=0):

    lstore= cbox.get_model()
    for i in entries:
        lstore.append([i])

    #print("append_combo_box", entries)
    cbox.set_active(active)

def get_text_value(widget):
    try:
        return float(widget.get_text())
    except:
        return 0.0

################## Widget factories ##################
def g_window_factory(title= None, width= 100, height= 100, idle_func= None, *args):
    w= Gtk.Window(type=WINDOW_TOPLEVEL)
    try:
        w.set_geometry_hints(min_width= width, min_height= height)
    except TypeError:
        h= gdk.Geometry()
        h.min_width= width
        h.min_height= height
        w.set_geometry_hints(None, h, gdk.WindowHints.MIN_SIZE)
    if title:
        w.set_title(title)

    w.connect('delete-event', Gtk.main_quit)

    if idle_func:
        GObject.idle_add(idle_func, *args)

    return w

def g_frame_factory(label= None):
    frame= Gtk.Frame()
    if label is not None:
        frame.set_label('<b>'+label+'</b>')
        l= frame.get_label_widget()
        l.set_use_markup(True)
    frame.set_shadow_type(SHADOW_ETCHED_OUT)

    return frame

# Gtk.Table is deprecated use Gtk.Grid
def g_table(n_rows, n_columns, homogeneous= False):
    if gtk3:
        table= Gtk.Table(n_rows=n_rows, n_columns=n_columns, homogeneous=homogeneous)
    else:
        table= Gtk.Table(n_rows, n_columns, homogeneous)
    return table

def g_table_factory(cells, homogeneous= False, col_spacing= 0, row_spacing= 0, xoptions=AOPT_EXPAND|AOPT_FILL, yoptions=AOPT_EXPAND|AOPT_FILL, xpadding=0, ypadding=0):
    rows= len(cells)
    cols= 0
    for r in range(rows):
        c= len(cells[r])
        if c > cols:
            cols= c

    table= g_table(n_rows=rows, n_columns=cols, homogeneous=homogeneous)

    table.set_row_spacings(row_spacing)
    table.set_col_spacings(col_spacing)

    for r in range(rows):
        for c in range(len(cells[r])):
            if cells[r][c] is not None:
                table.attach(cells[r][c], c, c+1, r, r+1, xoptions, yoptions, xpadding, ypadding)

    return table

def g_cbox_factory(entries, active=0, callback= None, cb_data= None):
    cbox= Gtk.ComboBox()
    populate_combo_box(cbox, entries, active)
    if callback is not None:
        if cb_data is not None:
            cbox.connect('changed', callback, cb_data)
        else:
            cbox.connect('changed', callback)

    return cbox

def g_spin_factory(value=0.0, vmin= 0.0, vmax= 10.0, up= 1.0, pup= 10.0, width= 10, digits= 0, x_align= 1.0, callback= None, cb_data= None):
    sbtn= Gtk.SpinButton(digits= digits)
    sbtn.set_numeric(True)
    sbtn.set_range(vmin, vmax)
    sbtn.set_increments(up, pup)
    sbtn.set_width_chars(width)
    sbtn.set_alignment(x_align)
    sbtn.set_value(value)

    if callback is not None:
        if cb_data is not None:
            sbtn.connect('value-changed', callback, cb_data)
        else:
            sbtn.connect('value-changed', callback)
    return sbtn

def g_entry_factory(text= '0', width= 10, x_align= 1.0, callback= None, cb_data= None):
    entry= Gtk.Entry()
    entry.set_text(str(text))
    entry.set_width_chars(width)
    entry.set_alignment(x_align)
    entry.get_value= MethodType(get_text_value, entry)

    if callback is not None:
        if cb_data is not None:
            entry.connect('activate', callback, cb_data)
        else:
            entry.connect('activate', callback)

    return entry

def g_label(label= ''):
    if gtk3:
        w= Gtk.Label(label=label)
    else:
        w= Gtk.Label(label)
    return w

def g_label_factory(text= '0', width= 10, x_align= 1.0, frame= False):
    label= Gtk.Label()
    label.set_use_markup(True)
    label.set_markup(str(text))
    if width is not None:
        label.set_width_chars(width)
    try:
        label.set_xalign(x_align)
        label.set_yalign(0.5)
    except AttributeError:
        label.set_alignment(x_align, 0.5)
    label.get_value= MethodType(get_text_value, label)

    if frame:
        f= g_frame_factory()
        f.add(label)
        return (f, label)
    else:
        return (label, label)

def g_separator_label_factory(text= 'Label', x_align= 0.5, v_align= 0.5):
    label= Gtk.Label()
    label.set_text(str(text))
    try:
        label.set_xalign(x_align)
        label.set_yalign(v_align)
    except AttributeError:
        label.set_alignment(x_align, v_align)
    hbox= Gtk.HBox(homogeneous=False, spacing=5)
    hsep= Gtk.HSeparator()
    hbox.pack_start(hsep, True, True, 0)
    hbox.pack_start(label, True, True, 0)
    hsep= Gtk.HSeparator()
    hbox.pack_start(hsep, True, True, 0)

    hbox.label= label

    return hbox

def g_button_factory(label= '', callback= None, cb_data= None):
    button= Gtk.Button(label= label)
    if callback is not None:
        if cb_data is not None:
            button.connect('clicked', callback, cb_data)
        else:
            button.connect('clicked', callback)

    return button

def g_cbutton(label='', stock=False):
    if gtk3:
        w= Gtk.CheckButton(label=label, stock=stock)
    else:
        w= Gtk.CheckButton(label, stock)
    return w

def g_cbutton_factory(label= '', callback= None, connectClicked= True, cb_data= None):
    button= g_cbutton(label= label)
    if callback is not None:
        if connectClicked:
            if cb_data is not None:
                button.connect('clicked', callback, cb_data)
            else:
                button.connect('clicked', callback)
        else:
            if cb_data is not None:
                button.connect('toggled', callback, cb_data)
            else:
                button.connect('toggled', callback)

    return button

def g_arrowbutton_factory(direction, callback= None, cb_data= None):
    button= Gtk.Button()
    arrow= Gtk.Arrow(direction, SHADOW_ETCHED_OUT)
    button.add(arrow)
    if callback is not None:
        if cb_data is not None:
            button.connect('clicked', callback, cb_data)
        else:
            button.connect('clicked', callback)
    return button

def g_update_entry(widget, value, format_str= None):
    if widget.is_focus():
        return

    if format_str is None:
        text= str(value)
    else:
        text= format_str.format(value)

    widget.set_text(text)


################## LoginDialog ##################

class LoginDialog(Gtk.Dialog):
    def __init__(self, parent= None, user= None, by_location= False):

        if gtk3:
            super(LoginDialog, self).__init__(title= 'Login',
                                              transient_for= parent,
                                              modal=True,
                                              destroy_with_parent=True)

            self.add_buttons(Gtk.STOCK_CANCEL, RESPONSE_REJECT,
                             Gtk.STOCK_OK, RESPONSE_ACCEPT)
        else:
            super(LoginDialog, self).__init__(title= 'Login',
                                              parent= parent,
                                              flags= DIALOG_MODAL | DIALOG_DESTROY_WITH_PARENT,
                                              buttons=(Gtk.STOCK_CANCEL, RESPONSE_REJECT,
                                                       Gtk.STOCK_OK, RESPONSE_ACCEPT))

        table= g_table(n_rows=2, n_columns=3, homogeneous= False)

        label= g_label(label= 'User')
        table.attach(label, 0, 1, 0, 1, xoptions=AOPT_FILL, yoptions=AOPT_FILL)
        label= g_label(label= 'Password',)
        table.attach(label, 0, 1, 1, 2, xoptions=AOPT_FILL, yoptions=AOPT_FILL)

        self.user_entry = Gtk.Entry()
        if user is not None:
            self.user_entry.set_text(str(user))
        table.attach(self.user_entry, 1, 2, 0, 1, xoptions=AOPT_FILL, yoptions=AOPT_FILL)

        self.password_entry = Gtk.Entry()
        self.password_entry.set_visibility(False)
        table.attach(self.password_entry, 1, 2, 1, 2, xoptions=AOPT_FILL, yoptions=AOPT_FILL)
        self.password_entry.connect("activate", lambda ent, dlg, resp: dlg.response(resp),
                      self, RESPONSE_ACCEPT)

        if by_location:
            label= g_label(label= 'By Location')
            table.attach(label, 0, 1, 2, 3, xoptions=AOPT_FILL, yoptions=AOPT_FILL)
            self.by_location_cbtn= g_cbutton()
            table.attach(self.by_location_cbtn, 1, 2, 2, 3, xoptions=AOPT_FILL, yoptions=AOPT_FILL)
        else:
            self.by_location_cbtn= None

        self.vbox.pack_end(table, True, True, 0)

        self.vbox.show_all()

    def set_user(self, text):
        self.user_entry.set_text(text)

    def run(self, user= None):
        if user is not None:
            self.user_entry.set_text(str(user))

        self.password_entry.grab_focus()

        result = super(LoginDialog, self).run()

        if result == RESPONSE_ACCEPT:
            user = self.user_entry.get_text()
            password = self.password_entry.get_text()
            if self.by_location_cbtn is not None:
                by_location= self.by_location_cbtn.get_active()
            else:
                by_location= False
        else:
            user = None
            password= None
            by_location= False

        self.hide()

        return (user, password, by_location)


################## StatusLed ##################

LED_OFF  = 0
LED_GRAY = 0
LED_GREY = 0

LED_ON   = 1
LED_GREEN= 1

LED_ERROR= 2
LED_RED  = 2

class StatusLed(Gtk.HBox):

    def __init__(self, *args):
        super(StatusLed, self).__init__()
        self.active= LED_GRAY

        if len(args) <1:
            folder= os.path.dirname(__file__)
            #print folder
            images= [folder+'/led_gray.xpm', folder+'/led_green.xpm', folder+'/led_red.xpm']
        else:
            images= args

        self.img= []
        for im in images:
            gim= Gtk.Image()
            gim.set_from_file(im)
            self.img.append(gim)
            self.img[-1].set_no_show_all(True)
            self.pack_start(self.img[-1], False, True, 0)

        self.img[self.active].show()

    def set_active(self, which):
        self.img[self.active].hide()
        self.active= which
        self.img[self.active].show()

    def get_active(self):
        return self.active

    def toggle(self):
        if self.active == 0:
            self.set_active(LED_GREEN)
        else:
            self.set_active(LED_GRAY)

################## TimeRangeChooser ##################

class TimeRangeChooser:

    # The month is gtk.Calendar is [0:11]
    # Days and years are however normal i.e. [1:31], 2012 is 2012

    def __init__(self, title= 'Time range chooser', parent= None, date= None,
                 start= '12:00:00', end= '+01:00:00', user_area= None):
        self._build_widget(title, user_area)

        if date != None:
            self.SetDate(date)
        self.SetStartTime(start)
        self.SetEndTime(end)

    def _build_widget(self, title, user_area= None):
        ''' A GTKCalendar plus two GTKEntry (t1, t2). Parameters ex: date= 2012-01-15, time1= 22:00:00 time2= 23:00:00/+-01:00:00 '''

        self.calendar = Gtk.Calendar()

        self.time1_entry= Gtk.Entry()
        self.time1_entry.set_width_chars(10)
        self.time1_entry.set_alignment(1.0)

        self.time2_entry= Gtk.Entry()
        self.time2_entry.set_width_chars(10)
        self.time2_entry.set_alignment(1.0)

        hbox= Gtk.HBox()

        vbox= Gtk.VBox()
        vbox.add(g_label(label='Start'))
        vbox.add(self.time1_entry)
        hbox.add(vbox)

        vbox= Gtk.VBox()
        vbox.add(g_label(label='End/Delta'))
        vbox.add(self.time2_entry)
        hbox.add(vbox)

        vbox= Gtk.VBox()
        vbox.add(self.calendar)
        vbox.add(hbox)

        # The user may want to add something below the chooser
        if user_area != None:
            vbox.add(user_area)

        vbox.show_all()

        self.dialog = Gtk.Dialog(title=title,
                                 modal=True,
                                 destroy_with_parent=True)

        self.dialog.add_buttons(Gtk.STOCK_CANCEL, RESPONSE_REJECT,
                         Gtk.STOCK_OK, RESPONSE_ACCEPT)

        self.dialog.vbox.pack_start(vbox, True, True, 0)


    def SetDate(self, date):
        ''' Format is yyy-mm-dd '''

        if date != None:
            tmp= [int(i) for i in date.split('-')]
            self.calendar.select_month(tmp[1]-1, tmp[0])
            self.calendar.select_day(tmp[2])

    def GetDate(self):
        ''' return format is yyy-mm-dd '''

        date= self.calendar.get_date()
        return '{0:4d}-{1:02d}-{2:02d}'.format(date[0], date[1]+1, date[2])

    def SetStartTime(self, time):
        self.time1_entry.set_text(time)

    def GetStartTime(self):
        return self.time1_entry.get_text()

    def SetEndTime(self, time):
        self.time2_entry.set_text(time)

    def GetEndTime(self):
        return self.time2_entry.get_text()

    def Run(self):

        self.dialog.present()

        response = self.dialog.run()

        if response == RESPONSE_ACCEPT:
            date= self.calendar.get_date()
            t2_str= self.time2_entry.get_text().split(':')
            t1= [int(i) for i in self.time1_entry.get_text().split(':')]
            t2_str= self.time2_entry.get_text().split(':')
            t2= [int(i) for i in t2_str]

            self.dialog.hide()

            start= dt.datetime(date[0], date[1]+1, date[2], t1[0], t1[1], t1[2])

            delta= 0
            if t2_str[0][0] == '-':
                delta= -(-t2[0]*60*60 + t2[1]*60 + t2[2])
            elif t2_str[0][0] == '+':
                delta= t2[0]*60*60 +t2[1]*60 + t2[2]

            if delta != 0:
                end= start+dt.timedelta(seconds= delta)
            else:
                end= dt.datetime(date[0], date[1]+1, date[2], t2[0], t2[1], t2[2])

            return (start, end)

        else:
            self.dialog.hide()
            return (None, None)


################## DeviceChooser ##################

class DeviceChooser(Gtk.Dialog):
    ''' Creates a dialog with a vbox of check_buttons according to the
    list of devices passed. Each device is a list of type (name, checked) '''

    def __init__(self, devices, title= 'Device chooser', parent= None, frame_label= 'Devices', message= None):
        if gtk3:
            super(DeviceChooser, self).__init__(title= title,
                                                transient_for= parent,
                                                modal=True,
                                                destroy_with_parent=True)
            self.add_buttons(Gtk.STOCK_CANCEL, RESPONSE_REJECT,
                             Gtk.STOCK_OK, RESPONSE_ACCEPT)
        else:
            super(DeviceChooser, self).__init__(title= title,
                                                parent= parent,
                                                flags= DIALOG_MODAL | DIALOG_DESTROY_WITH_PARENT,
                                                buttons=(Gtk.STOCK_CANCEL, RESPONSE_REJECT,
                                                Gtk.STOCK_OK, RESPONSE_ACCEPT))

        self.buttons= []
        self.build_widget(devices, frame_label, message)


    def build_widget(self, devices, frame_label, message):
        ''' Creates a table with one row per device.
        Each row contains a label and a check button '''
        print(devices, frame_label, message)
        if message is not None:
            frame, message_label= g_label_factory(message, width= None, x_align= 0.5, frame= True)
            frame.set_border_width(5)
            message_label.set_line_wrap(True)
            message_label.set_justify(JUSTIFY_FILL)
            message_label.set_padding(10, 10)
            frame.show_all()
            self.vbox.pack_start(frame, True, True, 0)

        frame= Gtk.Frame(label='<b>{:s}</b>'.format(frame_label))
        frame.set_border_width(5)
        frame.get_label_widget().set_use_markup(True)
        frame.set_shadow_type(SHADOW_ETCHED_OUT)

        vbox = Gtk.VBox(homogeneous=True, spacing=1)
        vbox.set_border_width(5)
        for dev in devices:
            if is_string(dev):
                checkbtn= g_cbutton(label=dev, stock=False)
                checkbtn.set_active(True)
            else:
                checkbtn= g_cbutton(label=dev[0], stock=False)
                checkbtn.set_active(dev[1])

            vbox.pack_start(checkbtn, False, True, 0)
            self.buttons.append(checkbtn)

        vbox.pack_start(Gtk.HSeparator(), True, True, 0)

        hbox= Gtk.HBox(homogeneous=True)
        hbox.pack_start(g_button_factory('All', self.on_all_cb), True, True, 0)
        hbox.pack_start(g_button_factory('None', self.on_none_cb), True, True, 0)
        vbox.pack_start(hbox, True, True, 0)

        frame.add(vbox)
        frame.show_all()

        self.vbox.pack_start(frame, True, True, 0)

    def on_all_cb(self, widget):
        for button in self.buttons:
            button.set_active(True)

    def on_none_cb(self, widget):
        for button in self.buttons:
            button.set_active(False)

    def SetActive(self, dev_indx, state):
        ''' Set the state of a device '''

        if dev_indx >= 0 and dev_indx < len(self.buttons):
            self.buttons[dev_indx].set_active(state)

    def GetActive(self, dev_indx):
        ''' Get the state of a device '''

        if dev_indx >= 0 and dev_indx < len(self.buttons):
            return self.buttons[dev_indx].get_active()

        return False

    def GetSelected(self):
        ''' Get the selection state of the devices '''

        selected= []

        for button in self.buttons:
            selected.append(button.get_active())

        return selected

    def SetSelected(self, selected):
        ''' Set the selection state of the devices '''

        for i in range(min(len(selected), len(self.buttons))):
            self.buttons[i].set_active(selected[i])

    def Run(self):

        old_sel= self.GetSelected()

        self.present()

        response= self.run()

        self.hide()

        if response == RESPONSE_ACCEPT:
            return self.GetSelected()

        else:
            self.SetSelected(old_sel)
            return old_sel


################## SingleDeviceChooser ##################

class SingleDeviceChooser(Gtk.Dialog):
    ''' Creates a dialog with a combo box populated with the devices list. Run returns the selected item or -1 '''

    def __init__(self, devices, active= -1, title= 'Device chooser', parent= None, frame_label= 'Devices', message= None):
        if gtk3:
            super(SingleDeviceChooser, self).__init__(title= title,
                                                      transient_for= parent,
                                                      modal=True,
                                                      destroy_with_parent=True)
            self.add_buttons(Gtk.STOCK_CANCEL, RESPONSE_REJECT,
                             Gtk.STOCK_OK, RESPONSE_ACCEPT)
        else:
            super(SingleDeviceChooser, self).__init__(title= title,
                                                      parent= parent,
                                                      flags= DIALOG_MODAL | DIALOG_DESTROY_WITH_PARENT,
                                                      buttons=(Gtk.STOCK_CANCEL, RESPONSE_REJECT,
                                                               Gtk.STOCK_OK, RESPONSE_ACCEPT))


        self.buttons= []
        self.build_widget(devices, active, frame_label, message)


    def build_widget(self, devices, active, frame_label, message):

        #print devices, frame_label, message
        if message is not None:
            frame, message_label= g_label_factory(message, width= None, x_align= 0.5, frame= True)
            frame.set_border_width(5)
            message_label.set_line_wrap(True)
            message_label.set_justify(JUSTIFY_FILL)
            message_label.set_padding(10, 10)
            self.vbox.pack_start(frame, True, True, 0)

        frame= Gtk.Frame(label='<b>{:s}</b>'.format(frame_label))
        frame.set_border_width(5)
        frame.get_label_widget().set_use_markup(True)
        frame.set_shadow_type(SHADOW_ETCHED_OUT)

        self.cbox= Gtk.ComboBox()

        populate_combo_box(self.cbox, devices, active)
        frame.add(self.cbox)
        self.vbox.pack_start(frame, True, True, 0)

        self.vbox.show_all()


    def GetSelected(self):
        ''' Get the selected item '''

        return self.cbox.get_active()

    def SetSelected(self, selected):
        ''' Set the selected item '''

        self.cbox.set_active(selected)

    def Run(self):

        old_sel= self.GetSelected()

        self.present()

        response= self.run()

        self.hide()

        if response == RESPONSE_ACCEPT:
            return self.GetSelected()

        else:
            self.SetSelected(old_sel)
            return old_sel


################## TView ##################

class TView(Gtk.TreeView):

    def __init__(self, columns= None, callback= None, multiple_selection= False):
        super(TView, self).__init__()

        if columns is None:
            columns= ['Column 1']

        types= []
        for i, c in enumerate(columns):
            types.append(str)

        liststore=  Gtk.ListStore(*types)
        self.set_model(liststore)

        for i, c in enumerate(columns):
            if c[0] != '-':
                tvcolumn= Gtk.TreeViewColumn(c)
                self.append_column(tvcolumn)
                cell= Gtk.CellRendererText()
                tvcolumn.pack_start(cell, True)
                tvcolumn.set_sort_column_id(i)
                tvcolumn.set_attributes(cell, text=i)

        # make treeview searchable
        self.set_search_column(0)
        self.set_enable_search(True)

        selection= self.get_selection()
        if multiple_selection:
            selection.set_mode(SELECTION_MULTIPLE)
        if callback is not None:
            selection.connect('changed', callback)

        self.show_all()


    def append(self, rows= None):

        if rows is None:
            return

        liststore= self.get_model()

        if len(rows) > 1000:
            self.set_model(None)

        for r in rows:
            liststore.append(r)

        if len(rows) > 1000:
            self.set_model(liststore)

    def clear(self):
        self.get_model().clear()

    def get_selected(self):
        selection= self.get_selection()

        if selection is not None:
            if selection.get_mode() in([SELECTION_SINGLE, SELECTION_BROWSE]) :
                selected= selection.get_selected()
                if selected is not None and selected[1] is not None:
                    liststore= selected[0]
                    iter= selected[1]
                    res= [iter]
                    for col in range(liststore.get_n_columns()):
                        res.append(liststore.get_value(iter, col))
                    return [res]
            if selection.get_mode() == SELECTION_MULTIPLE:
                selected= selection.get_selected_rows()
                if selected is not None and len(selected[1]) > 0:
                    liststore= selected[0]
                    n_cols= liststore.get_n_columns()
                    res= []
                    for s in selected[1]:
                        iter= liststore.get_iter(s)
                        values= [iter]
                        for col in range(n_cols):
                            values.append(liststore.get_value(iter, col))
                        res.append(values)
                    return res

        return None



################## FileChooser ##################

FILE_OPEN   = 0
DIR_SELECT  = 1
FILE_SAVE   = 2
FILE_SAVE_AS= 3
DIR_CREATE  = 4

def ChooseFile(parent= None, title= None, mode= FILE_OPEN, start_with= None, extra_widget= None):

    if mode == FILE_OPEN:
        action= FILE_CHOOSER_ACTION_OPEN
        stock= Gtk.STOCK_OPEN
        if title is None:
            title= 'Open File'
    elif mode == DIR_SELECT:
        action= FILE_CHOOSER_ACTION_SELECT_FOLDER
        stock= Gtk.STOCK_OPEN
        if title is None:
            title= 'Select Directory'
    elif mode == FILE_SAVE or mode == FILE_SAVE_AS:
        action= FILE_CHOOSER_ACTION_SAVE
        stock= Gtk.STOCK_SAVE
        if title is None:
            title= 'Save As'
    elif mode == DIR_CREATE:
        action= FILE_CHOOSER_ACTION_CREATE_FOLDER
        stock= Gtk.STOCK_NEW
        if title is None:
            title= 'Create Directory'
    else:
        raise ValueError("Unknown mode for ChooseFile")

    dialog= Gtk.FileChooserDialog(title= title, parent= parent,
                                  action= action,
                                  buttons=(Gtk.STOCK_CANCEL, RESPONSE_CANCEL,
                                           stock, RESPONSE_ACCEPT))

    if start_with is not None:
        path, name= os.path.split(start_with)
        dialog.set_current_folder(path)
        if mode == FILE_OPEN:
            dialog.set_filename(start_with)
        if mode == FILE_SAVE or mode == FILE_SAVE_AS or mode == DIR_CREATE:
            dialog.set_current_name(name)

    if extra_widget is not None:
        dialog.set_extra_widget(extra_widget)
        extra_widget.show_all()

    dialog.set_resizable(True)

    if dialog.run() == RESPONSE_ACCEPT:
        filename = dialog.get_filename()
    else:
        filename = None

    dialog.destroy()

    return filename
